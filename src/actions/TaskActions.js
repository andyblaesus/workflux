import AppDispatcher from '../dispatcher/AppDispatcher'
import TaskConstants from '../constants/TaskConstants'

function addTask(taskContent) {
  AppDispatcher.dispatch({
    actionType: TaskConstants.TASK_ADD,
    taskContent: taskContent,
  })
}
const TaskActions = {
  addTask
}
export default TaskActions