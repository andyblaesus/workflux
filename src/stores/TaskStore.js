import AppDispatcher from '../dispatcher/AppDispatcher'
import EventEmitter from 'events'
import TaskConstants from '../constants/TaskConstants'

const UPDATE = 'UPDATE'

let _tasks = []

function addTask(taskContent) {
  _tasks.push({
    id: String(Math.random()),
    content: taskContent,
  })
}

const TaskStore = Object.assign({}, EventEmitter.prototype, {
  getState() {
    return _tasks
  },

  emitUpdate() {
    this.emit(UPDATE)
  },

  addUpdateListener(callback) {
    this.addListener(UPDATE, callback)
  },

  removeUpdateListener(callback) {
    this.removeListener(UPDATE, callback)
  },
})

AppDispatcher.register(action => {
  switch(action.actionType) {
    case TaskConstants.TASK_ADD:
      addTask(action.taskContent)
      TaskStore.emitUpdate()
      break

    default: // no op
  }
})

export default TaskStore
