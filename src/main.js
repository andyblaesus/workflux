import React from 'react'
import ReactDOM from 'react-dom'
import WorkfluxApp from './components/WorkfluxApp'

let mountPoint = document.getElementById('content')
ReactDOM.render(<WorkfluxApp />, mountPoint)
