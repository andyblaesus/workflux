import React from 'react'
import TaskList from './TaskList'
import TaskInput from './TaskInput'
import TaskActions from '../actions/TaskActions'
import TaskStore from '../stores/TaskStore'

class WorkfluxApp extends React.Component {

  constructor(props) {
    super(props)
    this.state = {tasks: []}
  }

  componentDidMount() {
    TaskStore.addUpdateListener(this.updateState.bind(this))
  }

  componentWillUnmount() {
    TaskStore.removeUpdateListener(this.updateState.bind(this))
  }

  updateState() {
    this.setState({
      tasks: TaskStore.getState()
    })
  }

  render() {
    return (
      <div>
        <TaskList tasks={this.state.tasks} />
        <TaskInput onSave={TaskActions.addTask}/>
      </div>
    )
  }
}

export default WorkfluxApp
