import React from 'react'

const ENTER_KEY_CODE = 13

class TaskInput extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: this.props.initialContent
    }
  }

  handleKeyDown(event) {
    if (event.keyCode === ENTER_KEY_CODE) {
      this.props.onSave(this.state.value)
      this.setState({
        value: ''
      })
    }
  }

  handleChange(event) {
    this.setState({
      value: event.target.value
    })
  }

  render() {
    return <input type='text'
                  autoFocus
                  onKeyDown={this.handleKeyDown.bind(this)}
                  onChange={this.handleChange.bind(this)}
                  value={this.state.value}
           />
  }
}

TaskInput.propTypes = {
  onSave: React.PropTypes.func,
  initialContent: React.PropTypes.string,
}

TaskInput.defaultProps = {
  initialContent: ''
}

export default TaskInput