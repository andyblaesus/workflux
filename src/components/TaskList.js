import React from 'react'

const TaskList = ({tasks}) =>
  <ul className='tasklist'>
    {console.log(tasks)}
    {tasks.map(task => (
      <li key={task.id}>
        {task.content}
      </li>
    ))}
  </ul>

export default TaskList

